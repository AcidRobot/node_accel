var controller = require('http').createServer(handler);
var io = require('socket.io').listen(controller);
var fs = require('fs');
var five = require('johnny-five');
var board = new five.Board()

controller.listen(8080);

function handler (req, res) {
   fs.readFile(__dirname + '/index.html',
    function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading index.html');
      }
      console.log('Connection');
      res.writeHead(200);
      res.end(data);
      }
   );
}
//turn off debug
io.set('log level', 1);

//set board to ready state to start transfer of data
board.on('ready', function () {

  // Motor
  var motor1 = new five.Motor({
    pin: 10,
  });
// digital pins needed to tell the motor controller "forward" and "backword"
  var digitalM1a = new five.Pin(4);

  var digitalM1b = new five.Pin(2);


// define interactions with client
  io.sockets.on('connection', function(socket)
  {
      //send data to client
      setInterval(function()
      {
          socket.emit('date', {'date': new Date()});
      }, 1000);

      //recieve client data
      socket.on('client_data', function(data)
      {
          process.stdout.write(data.letter);
      });

//tilt left right Max +-90 
      // socket.on('gamma', function (data)
      // {
      //   var gamma = data.gamma * 1.416;
      //   var posGamma = gamma * (-1);
      //   console.log('gamma:');
      //   console.log(data.gamma);
      //
      //   if(gamma > 10)
      //   {
      //     motor1.start(gamma);
      //     digitalM1a.low();
      //     digitalM1b.high();
      //   }
      //   else if(gamma < (-10))
      //   {
      //     motor1.start(posGamma);
      //     digitalM1a.high();
      //     digitalM1b.low();
      //   }
      //   else
      //   {
      //     motor1.stop();
      //     digitalM1a.low();
      //     digitalM1b.low();
      //   }
      // });

// Tilt forward / backward Max +-180
     socket.on('beta', function (data)
     {
        var beta = data.beta * 1.416;
        var posBeta = beta * (-1);
        console.log('beta:');
        console.log(data.beta);

        if(beta > 10)
        {
          motor1.start(beta);
          digitalM1a.low();
          digitalM1b.high();
        }
        else if(beta < (-10))
        {
          motor1.start(posBeta);
          digitalM1a.high();
          digitalM1b.low();
        }
        else
        {
          motor1.stop();
          digitalM1a.low();
          digitalM1b.low();
        }

     });

//compass Max 0-360
    //  socket.on('alpha', function (data)
    //  {
    //     console.log('alpha:');
    //     console.log(data.alpha);
    //     var alpha = data.alpha * 1.416;
    //     var posAlpha = alpha * (-1);
    //
    //     if(alpha > 10)
    //     {
    //       motor1.start(alpha);
    //       digitalM1a.low();
    //       digitalM1b.high();
    //     }
    //     else if(alpha < (-10))
    //     {
    //       motor1.start(posAlpha);
    //       digitalM1a.high();
    //       digitalM1b.low();
    //     }
    //     else
    //     {
    //       motor1.stop();
    //       digitalM1a.low();
    //       digitalM1b.low();
    //     }
    // });

  });
})
