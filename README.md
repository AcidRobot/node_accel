# node_accel - Accelerometer data through a server - nodejs and socketIO
Nodejs, SocketIO, Accelerometer
This utilizes socket IO to send accelerometer data to a centralized server.
<br>
Demo:
<br>
1. clone repo
<br>
2. run "npm install"
<br>
3. navigate to line 10 of index.html and change to server IP address
<br>
4. run "node server.js"
<br>
5. navigate to "http://<serverIP>:8080"
<br>
6. Click "Feed Accelerometer"
<br>
7. If your device supports it you will now see accelerometer data
<br>
!!
